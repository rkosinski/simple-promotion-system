### Simle promotions system

Its simple promotions system written in plain PHP with PSR-4 composer autoload and PHPSpec tests.
Promotions can be stacked (sum).

#### Installation

Simple type: `composer install` inside project directory and run `php -S localhost:8001 -t public/` to run PHP built-in server.
Tests can be run by `bin/phpspec run`
