<?php declare(strict_types=1);

use App\Entity\User;
use App\Promotion\ShopPromotionSupervisor;
use App\Promotion\LevelPromotion;
use App\Promotion\PowerUserPromotion;

require __DIR__.'/../vendor/autoload.php';

// In full application promotions will be added as services, tagged, and loaded via CompilerPass (Symfony)
$supervisor = new ShopPromotionSupervisor();
$supervisor->addPromotion(new LevelPromotion());
$supervisor->addPromotion(new PowerUserPromotion());

$newUser = new User(5, false);
$powerUser = new User(14, true);

$newUserPromotions = $supervisor->getPromotion($newUser);
$powerUserPromotions = $supervisor->getPromotion($powerUser);

echo 'New user promotion percent: ' . $newUserPromotions->getPromotionPercent();
echo '<br/>';
echo 'Power user promotion percent: ' . $powerUserPromotions->getPromotionPercent();
