<?php declare(strict_types=1);

namespace spec\App\Promotion;

use App\Entity\UserInterface;
use PhpSpec\ObjectBehavior;

class PowerUserPromotionSpec extends ObjectBehavior
{
    public function it_can_return_promotion_for_power_user(UserInterface $user)
    {
        $user->isPowerUser()->willReturn(true)->shouldBeCalled();
        $this->getPromotionPercent($user)->shouldReturn(30);
    }
}
