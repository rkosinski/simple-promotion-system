<?php declare(strict_types=1);

namespace spec\App\Promotion;

use App\Entity\UserInterface;
use PhpSpec\ObjectBehavior;

class LevelPromotionSpec extends ObjectBehavior
{
    public function it_can_return_promotion_for_new_user(UserInterface $user)
    {
        $user->getLevel()->willReturn(5)->shouldBeCalled();
        $this->getPromotionPercent($user)->shouldReturn(5);
    }

    public function it_can_return_for_more_experienced_user(UserInterface $user)
    {
        $user->getLevel()->willReturn(10)->shouldBeCalled();
        $this->getPromotionPercent($user)->shouldReturn(10);
    }
}
