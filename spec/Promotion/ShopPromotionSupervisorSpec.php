<?php declare(strict_types=1);

namespace spec\App\Promotion;

use App\Entity\UserInterface;
use App\Promotion\LevelPromotion;
use App\Promotion\PowerUserPromotion;
use PhpSpec\ObjectBehavior;

class ShopPromotionSupervisorSpec extends ObjectBehavior
{
    public function it_can_return_sum_of_added_promotions(UserInterface $user)
    {
        $user->getLevel()->willReturn(5)->shouldBeCalled();
        $user->isPowerUser()->willReturn(true)->shouldBeCalled();

        $this->addPromotion(new LevelPromotion());
        $this->addPromotion(new PowerUserPromotion());

        $this->getPromotion($user)->getPromotionPercent()->shouldReturn(35);
    }
}
