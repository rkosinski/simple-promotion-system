<?php declare(strict_types=1);

namespace spec\App\DTO;

use PhpSpec\ObjectBehavior;

class ShopPromotionDTOSpec extends ObjectBehavior
{
    public function it_should_return_that_promotion_is_available()
    {
        $this->beConstructedWith(20);
        $this->isPromotionAvailable()->shouldReturn(true);
    }

    public function it_should_return_that_promotion_is_not_available()
    {
        $this->beConstructedWith(0);
        $this->isPromotionAvailable()->shouldReturn(false);
    }
}
