<?php declare(strict_types=1);

namespace spec\App\DTO;

use PhpSpec\ObjectBehavior;

class ShopLevelPromotionDTOSpec extends ObjectBehavior
{
    public function it_should_throw_exception_on_invalid_max_level()
    {
        $this->beConstructedWith(10, 5, 3);
        $this->shouldThrow('\InvalidArgumentException')->duringInstantiation();
    }

    public function it_can_return_promotion_percent_on_valid_constructor_parameters()
    {
        $this->beConstructedWith(10, 5, 6);
        $this->getPromotionPercent()->shouldReturn(10);
    }
}
