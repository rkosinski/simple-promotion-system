<?php declare(strict_types=1);

namespace App\Entity;

class User implements UserInterface
{
    private $uuid;
    private $level;
    private $isPowerUser;

    public function __construct(int $level, bool $isPowerUser = false)
    {
        $this->uuid = uniqid();
        $this->level = $level;
        $this->isPowerUser = $isPowerUser;
    }

    public function getUUID() : string
    {
        return $this->uuid;
    }

    public function getLevel() : int
    {
        return $this->level;
    }

    public function isPowerUser() : bool
    {
        return $this->isPowerUser;
    }
}
