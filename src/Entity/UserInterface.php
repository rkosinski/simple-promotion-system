<?php declare(strict_types=1);

namespace App\Entity;

interface UserInterface
{
    public function getUUId() : string;
    public function getLevel() : int;
    public function isPowerUser() : bool;
}
