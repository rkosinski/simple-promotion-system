<?php declare(strict_types=1);

namespace App\DTO;

class ShopPromotionDTO
{
    private $promotionPercent;

    public function __construct(int $promotionPercent)
    {
        $this->promotionPercent = $promotionPercent;
    }

    public function getPromotionPercent() : int
    {
        return $this->promotionPercent;
    }

    public function isPromotionAvailable() : bool
    {
        return ($this->promotionPercent !== 0);
    }
}
