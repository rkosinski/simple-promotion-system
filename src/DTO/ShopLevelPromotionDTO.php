<?php declare(strict_types=1);

namespace App\DTO;

class ShopLevelPromotionDTO
{
    private $promotionPercent;
    private $fromLevel;
    private $toLevel;

    public function __construct(int $promotionPercent, int $fromLevel, int $toLevel)
    {
        if ($fromLevel >= $toLevel) {
            throw new \InvalidArgumentException('Max level cannot be smaller or equal to min level!');
        }

        $this->promotionPercent = $promotionPercent;
        $this->fromLevel = $fromLevel;
        $this->toLevel = $toLevel;
    }

    public function getPromotionPercent() : int
    {
        return $this->promotionPercent;
    }

    public function getFromLevel() : int
    {
        return $this->fromLevel;
    }

    public function getToLevel() : int
    {
        return $this->toLevel;
    }
}
