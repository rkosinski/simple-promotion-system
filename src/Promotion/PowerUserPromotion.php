<?php declare(strict_types=1);

namespace App\Promotion;

use App\Entity\UserInterface;

class PowerUserPromotion implements ShopPromotionInterface
{
    private const POWER_USER_PROMOTION_PERCENT = 30;

    public function getPromotionPercent(UserInterface $user) : int
    {
        return ($user->isPowerUser()) ? self::POWER_USER_PROMOTION_PERCENT : 0;
    }
}
