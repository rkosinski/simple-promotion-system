<?php declare(strict_types=1);

namespace App\Promotion;

use App\DTO\ShopPromotionDTO;
use App\Entity\UserInterface;

class ShopPromotionSupervisor
{
    /** @var ShopPromotionInterface[] */
    private $promotions = [];

    public function addPromotion(ShopPromotionInterface $promotion) : void
    {
        $this->promotions[] = $promotion;
    }

    public function getPromotion(UserInterface $user) : ShopPromotionDTO
    {
        $promotionPercent = 0;
        foreach ($this->promotions as $promotion) {
            $promotionPercent += $promotion->getPromotionPercent($user);
        }

        return new ShopPromotionDTO($promotionPercent);
    }
}
