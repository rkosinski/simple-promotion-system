<?php declare(strict_types=1);

namespace App\Promotion;

use App\DTO\ShopLevelPromotionDTO;
use App\Entity\UserInterface;

class LevelPromotion implements ShopPromotionInterface
{
    public function getPromotionPercent(UserInterface $user) : int
    {
        $promotionPercent = 0;
        $userLevel = $user->getLevel();

        foreach ($this->getShopLevelPromotions() as $promotion) {
            if ($userLevel >= $promotion->getFromLevel() && $userLevel < $promotion->getToLevel()) {
                $promotionPercent = $promotion->getPromotionPercent();
                break;
            }
        }

        return $promotionPercent;
    }

    /**
     * @return ShopLevelPromotionDTO[]
     */
    private function getShopLevelPromotions() : array
    {
        return [
            new ShopLevelPromotionDTO(5, 4, 8),
            new ShopLevelPromotionDTO(10, 8, 20),
        ];
    }
}
