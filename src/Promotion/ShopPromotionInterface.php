<?php declare(strict_types=1);

namespace App\Promotion;

use App\Entity\UserInterface;

interface ShopPromotionInterface
{
    public function getPromotionPercent(UserInterface $user) : int;
}
